**EEE3088_Team30**

This project's Hardware Attached on Top board will be used as a temperature sensor. This sensor will attach on top of the STM32 PCB board via the use of the standard pin headers. The temperature sensor is a cheap and reliable product that can be used to detect changes temperature in the workplace where specific and accurate temperatures are required for optimal performance. THereafter, it will alert the client of the change in temperature, therefore allowing for the necessary measures to take place in sufficient time.

The device is specifically for use in areas whereby accurate temperatures are required for optimal performance and to ensure safety within the workplace. The HAT device can be utilised in places such as factories, greenhouses, transportation trucks, and more.

The roles within the team in producing the temperature sensor HAT board are defined below:

TYBBIL001 : Power Management

HRTMOG014 : Microcontroller Interfacing and Controller

SBHREV001 : Sensing
